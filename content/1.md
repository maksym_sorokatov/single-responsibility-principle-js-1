### Single Responsibility Principle Refactoring Story
Welcome to refactoring story about Single Responsibility Principle (SRP) for SOLID training course.
This story is about proper decomposition of classes and methods to fit Single Responsibility Principle.

#### Contacts Book
In this story you will see example of web service application that is able to store and manage information
about people personal data and their contacts like phones, emails, addresses etc.
Contacts Book exposes services via REST interface which allows it's users to store and query
persons information and contacts.

Check README to get all required instructions about how to build and run Contacts Book application,
and call it's RESTfull services to store and query information about persons contacts.
