### Email service

Now let's focus on PersonService
which after series of refactorings represents main business logic component.
It is able to aggregate and orchestrate other services and delegate some particular operations to them.
However it also should be focused on single responsibility to be kind of facade service
and not implement some utility functions like email preparations.
Let's extract email greeting business logic to dedicated service focused on email template preparations and
email sending operations EmailService