### Need of application evolution and extension

At the first glance application is able to fulfill it's current business goals.
However each application is supposed to grow over the time.
Often during development lifecycle when first prototype is implemented or after several improvements or bug fixes,
You might notice that some classes become too complex and hard for understanding and support.
Moreover it will be near to impossible to speedup further development by splitting tasks between several
programmers or testers. And each new feature will require accurate and expensive regression testing.

One of approaches to avoid such pitfalls is to follow Single Responsibility design principle and perform code refactoring.
It is good to do when you have the picture of how the application should work but you feel that your classes become overwhelmed
by amount and complexity of implemented features. 